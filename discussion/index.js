console.log("Hi")

console.log(document);

const txtFirstName = document.querySelector("#txt-first-name");
console.log(txtFirstName);
// result: input

const txtLastName = document.querySelector("#txt-last-name");
console.log(txtLastName);

const fullName = document.querySelector("#span-full-name")
console.log(fullName)

//Event Listeners
//event can have shorthand of (e)

/*txtFirstName.addEventListener('keyup', (event) => {
	fullName.innerHTML = txtFirstName.value
	txtLastName.addEventListener('keyup', (event) => {
		fullName.innerHTML = txtFirstName.value + " "  + txtLastName.value
	})
})*/

/*txtFirstName.addEventListener('keyup', (event) => {
	console.log(event);
	console.log(event.target);
	console.log(event.target.value)
})

//stretch
const keyCodeEvent = (e) => {
	let kc = e.keyCode;
	if(kc === 65){
		e.target.value = null;
		alert('Someone Clicked a')
	}
}

txtFirstName.addEventListener("keyup", keyCodeEvent);*/


//Activity Solution
const keyCodeEvent = (event) => {
	fullName.innerHTML = txtFirstName.value
	txtLastName.addEventListener('keyup', (event) => {
		fullName.innerHTML = txtFirstName.value + " "  + txtLastName.value
	})
} 

txtFirstName.addEventListener('keyup', keyCodeEvent)